﻿//---------------------------------------------------------------------------
// Created by Tsin.
// 09.08.2017
//---------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace UniformGapPanelSolution.Controls
{
  public class UniformGapPanel : Panel
  {
    private readonly List<Visibility> ChildrenVisibilityStates = new List<Visibility>();

    #region DependencyProperties

    /// <summary>
    /// Empty space between children
    /// </summary>
    public static readonly DependencyProperty GapProperty = DependencyProperty.Register("Gap", typeof(double), typeof(UniformGapPanel),
      new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsArrange),
      new ValidateValueCallback(ValidateGap));

    public double Gap
    {
      get { return (double)GetValue(GapProperty); }
      set { SetValue(GapProperty, value); }
    }

    private static bool ValidateGap(object gap)
    {
      double value = (double)gap;
      return (!double.IsNaN(value) && (value >= 0.0 && !double.IsPositiveInfinity(value)));
    }

    /// <summary>
    /// Dimension of children stacking
    /// </summary>
    public static readonly DependencyProperty OrientationProperty = DependencyProperty.Register("Orientation", typeof(Orientation), typeof(UniformGapPanel),
      new FrameworkPropertyMetadata(Orientation.Vertical, FrameworkPropertyMetadataOptions.AffectsArrange));

    public Orientation Orientation
    {
      get { return (Orientation)GetValue(OrientationProperty); }
      set { SetValue(OrientationProperty, value); }
    }

    /// <summary>
    /// Last children over the MaximumChildrenCount will not be rendered
    /// </summary>
    public static readonly DependencyProperty MaximumChildrenCountProperty = DependencyProperty.Register("MaximumChildrenCount", typeof(int), typeof(UniformGapPanel),
      new FrameworkPropertyMetadata(8, FrameworkPropertyMetadataOptions.AffectsArrange, new PropertyChangedCallback(OnMaximumChildrenCountChanged)),
      new ValidateValueCallback(ValidateMaximumChildrenCount));

    public int MaximumChildrenCount
    {
      get { return (int)GetValue(MaximumChildrenCountProperty); }
      set { SetValue(MaximumChildrenCountProperty, value); }
    }

    private static void OnMaximumChildrenCountChanged(DependencyObject d, DependencyPropertyChangedEventArgs args)
    {
      UniformGapPanel control = d as UniformGapPanel;
      if (control == null)
        throw new InvalidCastException("'d' is not a UniformGapPanel");

      int actualChildrenCount = control.Children.Count;
      int desiredChildrenCount = (int)args.NewValue;
      
      //TODO: Implement disabling UIElements bigger than MaximumChildrenCount
      
      //for (int i = desiredChildrenCount; i < actualChildrenCount; i++)
      //{
      //  UIElement child = control.Children[i];
      //  if (child == null)
      //    continue;

      //  if (child.Visibility != Visibility.Collapsed)
      //    child.Visibility = Visibility.Collapsed;
      //}
    }
    private static bool ValidateMaximumChildrenCount(object maxChildrenCount)
    {
      int value = (int)maxChildrenCount;
      return value >= 0;
    }

    #endregion DependencyProperties

    protected override Size ArrangeOverride(Size finalSize)
    {
      if (Children.Count == 1)
        Children[0].Arrange(new Rect(0, 0, finalSize.Width, finalSize.Height));
      else if (Children.Count > 1)
      {
        if (Orientation == Orientation.Vertical)
          ArrangeChildrenVertical(finalSize);
        else
          ArrangeChildrenHorizontal(finalSize);
      }

      return base.ArrangeOverride(finalSize);
    }
    private void ArrangeChildrenVertical(Size finalSize)
    {
      double childHeightConstraint = (finalSize.Height - (Children.Count - 1) * Gap) / Children.Count;
      if (childHeightConstraint < 0.0)
        childHeightConstraint = 0.0;

      Size childConstraint = new Size(finalSize.Width, childHeightConstraint);

      //arrange first child without gap at top
      UIElement firstChild = Children[0];
      Rect firstChildRect = new Rect(0, 0, childConstraint.Width, childConstraint.Height);
      firstChild.Arrange(firstChildRect);

      //arrange last child without gap at bottom
      UIElement lastChild = Children[Children.Count - 1];
      Rect lastChildRect = new Rect(0, finalSize.Height - childConstraint.Height, childConstraint.Width, childConstraint.Height);
      lastChild.Arrange(lastChildRect);

      //arrange remaining children if they exists
      Point childArrangePoint = new Point(0, childConstraint.Height + Gap);
      for (int i = 1; i < Children.Count - 1; i++)
      {
        UIElement child = Children[i];
        if (child == null)
          continue;

        Rect childRect = new Rect(childArrangePoint.X, childArrangePoint.Y, childConstraint.Width, childConstraint.Height);
        child.Arrange(childRect);

        childArrangePoint.Y += childConstraint.Height + Gap;
      }
    }
    private void ArrangeChildrenHorizontal(Size finalSize)
    {
      double childWidthConstraint = (finalSize.Width - (Children.Count - 1) * Gap) / Children.Count;
      if (childWidthConstraint < 0.0)
        childWidthConstraint = 0.0;

      Size childConstraint = new Size(childWidthConstraint, finalSize.Height);

      //arrange first child without gap at left
      UIElement firstChild = Children[0];
      Rect firstChildRect = new Rect(0, 0, childConstraint.Width, childConstraint.Height);
      firstChild.Arrange(firstChildRect);

      //arrange last child without gap at right
      UIElement lastChild = Children[Children.Count - 1];
      Rect lastChildRect = new Rect(finalSize.Width - childConstraint.Width, 0, childConstraint.Width, childConstraint.Height);
      lastChild.Arrange(lastChildRect);

      //arrange remaining children if they exists
      Point childArrangePoint = new Point(childConstraint.Width + Gap, 0);
      for (int i = 1; i < Children.Count - 1; i++)
      {
        UIElement child = Children[i];
        if (child == null)
          continue;

        Rect childRect = new Rect(childArrangePoint.X, childArrangePoint.Y, childConstraint.Width, childConstraint.Height);
        child.Arrange(childRect);

        childArrangePoint.X += childConstraint.Width + Gap;
      }
    }
  }
}
